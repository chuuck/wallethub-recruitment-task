The goal is to write a parser in Java that parses web server access log file, loads the log to MySQL and checks if a given IP makes more than a certain number of requests for the given duration. 

# SQL QUERIES TASKS #
You can find my SQL queries in LogRepository class in log package.


Write MySQL query to find IPs that mode more than a certain number of requests for a given time period. Example:
```
SELECT i.*, COUNT(l.ipAddress_id) as counted FROM log l LEFT JOIN ip_address i ON i.id=l.ipAddress_id WHERE date BETWEEN '2017-01-01 01:00:12.763' AND '2017-01-01 01:00:59.410' GROUP BY ipAddress_id HAVING count(ipAddress_id) > 2
```


Write MySQL query to find requests made by a given IP. Example:
```
SELECT request FROM log WHERE id = (SELECT id FROM ip_address WHERE ip = '192.168.0.1');
```

# PATIENCE #

Please be patient during the logs to the database loading.

# REMARKS #

Inserting data can be optimized by using batch methods, not implemented because of lack of time.
Hibernate logging is disabled due to readability of result. This can be re-enabled in AbstractRepository and/or logging.properties.

# TESTS #

Unfortunately or fortunately I found a new refactoring idea at the end of my development time and some part of code is not fully covered by tests.

# ISSUES #

If there are any issues, please drop me an email: kazmierczak.jakubb@gmail.com

Thanks for your time and decisions!