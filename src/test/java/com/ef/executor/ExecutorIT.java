package com.ef.executor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertNotNull;

public class ExecutorIT {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(System.out);
    }

    /**
     * No assertions because at the end of execution app is closed
     * @throws IOException
     */
    @Test
    public void shouldExecuteActionsAndDisplayResults() throws IOException {
        Map<String, String> parameters = prepareParameters();
        Executor executor = new Executor();

        executor.execute(parameters);
    }

    private Map<String, String> prepareParameters() {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("duration", "hourly");
        parameters.put("threshold", "2");
        parameters.put("startDate", "2017-01-01.01:23:30.554");
        parameters.put("accesslog", getClass().getClassLoader().getResource("fileTest.txt").getPath());

        return parameters;
    }
}