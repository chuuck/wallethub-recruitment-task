package com.ef.log;

import org.junit.Test;

import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class LogParserTest {

    private final LogParser logParser = new LogParser();

    @Test
    public void shouldParseLogWhenProvidedLogIsCorrect() {
        String log = "2017-01-01 00:00:23.003|192.168.169.194|\"GET / HTTP/1.1\"|200|\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393\"";

        Map<String, String> result = logParser.parse(log);

        assertThat(result)
                .isNotEmpty()
                .containsKeys((String[]) Stream.of(LogChunks.values()).map(Enum::name).collect(Collectors.toList()).toArray(new String[0]))
                .containsValues("2017-01-01 00:00:23.003", "192.168.169.194", "GET / HTTP/1.1", "200", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393");
    }

    @Test
    public void shouldNotParseLogWhenProvidedLogIsIncorrect() {
        String log = "2017-01-01 99:00:23.003|192.168194|\"GET / HTTP/1.1\"|11111|\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14393\"";

        Map<String, String> result = logParser.parse(log);

        assertThat(result)
                .isEmpty();
    }
}