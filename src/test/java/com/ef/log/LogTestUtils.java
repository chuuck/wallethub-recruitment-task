package com.ef.log;

import com.google.common.collect.Maps;

import java.util.Map;

public class LogTestUtils {

    public static Map<String, String> prepareValidDataMap() {
        Map<String, String> dataMap = Maps.newHashMap();
        dataMap.put(LogChunks.DATE.toString(), "2017-01-01 00:00:11.763");
        dataMap.put(LogChunks.IP_ADDRESS.toString(), "192.168.234.82");
        dataMap.put(LogChunks.REQUEST.toString(), "\"GET / HTTP/1.1\"");
        dataMap.put(LogChunks.STATUS.toString(), "200");
        dataMap.put(LogChunks.USER_AGENT.toString(), "\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"");
        return dataMap;
    }

    public static Map<String, String> prepareInvalidDataMap() {
        Map<String, String> dataMap = Maps.newHashMap();
        dataMap.put(LogChunks.DATE.toString(), "2017-99-01 99:00:11.763");
        dataMap.put(LogChunks.IP_ADDRESS.toString(), "192.168.82");
        dataMap.put(LogChunks.REQUEST.toString(), "test");
        dataMap.put(LogChunks.STATUS.toString(), "5252");
        dataMap.put(LogChunks.USER_AGENT.toString(), "\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"");
        return dataMap;
    }
}
