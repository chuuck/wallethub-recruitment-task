package com.ef.log;

import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LogValidatorTest {

    private final LogValidator logValidator = new LogValidator();

    @Test
    public void shouldProperlyValidateIpAddressWhenValidDataIsProvided() {
        Map<String, String> dataMap = LogTestUtils.prepareValidDataMap();

        boolean isValid = logValidator.validate(dataMap);

        assertThat(isValid).isTrue();
    }

    @Test
    public void shouldNotProperlyValidateIpAddressWhenInvalidDataIsProvided() {
        Map<String, String> dataMap = LogTestUtils.prepareInvalidDataMap();

        boolean isValid = logValidator.validate(dataMap);

        assertThat(isValid).isFalse();
    }
}