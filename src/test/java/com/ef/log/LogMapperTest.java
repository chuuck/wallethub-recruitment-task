package com.ef.log;

import com.ef.log.dto.LogEntry;
import org.joda.time.IllegalFieldValueException;
import org.joda.time.LocalDateTime;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

public class LogMapperTest {

    private final LogMapper logMapper = new LogMapper();

    @Test
    public void shouldMapDataWhenLogIsValid() {
        Map<String, String> dataMap = LogTestUtils.prepareValidDataMap();

        LogEntry parsedLogEntry = logMapper.mapToLogEntry(dataMap);

        assertThat(parsedLogEntry)
                .isNotNull()
                .hasFieldOrPropertyWithValue("date", new LocalDateTime(Timestamp.valueOf("2017-01-01 00:00:11.763")))
                .hasFieldOrPropertyWithValue("ipAddress", "192.168.234.82")
                .hasFieldOrPropertyWithValue("request", "\"GET / HTTP/1.1\"")
                .hasFieldOrPropertyWithValue("status", "200")
                .hasFieldOrPropertyWithValue("userAgent", "\"swcd (unknown version) CFNetwork/808.2.16 Darwin/15.6.0\"");
    }
    @Test(expected = IllegalFieldValueException.class)
    public void shouldMapDataWhenLogIsInvalid() {
        Map<String, String> dataMap = LogTestUtils.prepareInvalidDataMap();

        logMapper.mapToLogEntry(dataMap);
    }
}