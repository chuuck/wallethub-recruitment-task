package com.ef.log;

import com.ef.ip.persistence.IPAddress;
import com.ef.log.persistence.Log;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Sets;
import org.joda.time.LocalDateTime;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.*;

@Ignore("In order to pass this test needs to be connected to the database.")
public class LogRepositoryIT {

    private LogRepository logRepository = new LogRepository();

    @Before
    public void before() {
        logRepository.removeAll();
    }

    @Test
    public void shouldInsertLog() {
        IPAddress ipAddress = new IPAddress();
        Log log = new Log();

        log.setDate(new LocalDateTime(Timestamp.valueOf("2017-01-01 00:00:11.763")));
        log.setRequest("GET");
        log.setStatus("200");
        log.setUserAgent("Mozilla");
        ipAddress.setIp("192.168.0.1");
        ipAddress.setBlocked(false);
        log.setIpAddress(ipAddress);

        logRepository.insertLog(log);

        Log result = logRepository.findLogById(1L);
        assertThat(result)
                .isNotNull()
                .hasFieldOrProperty("date")
                .hasFieldOrProperty("ipAddress")
                .hasFieldOrPropertyWithValue("request", "GET")
                .hasFieldOrPropertyWithValue("status", "200")
                .hasFieldOrPropertyWithValue("userAgent", "Mozilla");
    }
}