package com.ef.parameter;

import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.*;

public class ParamResolverTest {

    private ParamResolver paramResolver = new ParamResolver();

    @Test
    public void shouldResolveParametersWhenTheyAreProvided() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=100"};

        Map<String, String> results = paramResolver.resolve(params);

        assertThat(results).containsKeys("accesslog", "startDate", "duration", "threshold").containsValues("/path/to/file", "2017-01-01.13:00:00", "hourly", "100").size().isEqualTo(4);
    }

    @Test
    public void shouldNotResolveAllParametersWhenMoreThanExpectedParamsAreProvided() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=100", "--test=test"};


        Map<String, String> results = paramResolver.resolve(params);

        assertThat(results).isEmpty();
    }

    @Test
    public void shouldNotResolveParametersWhenAllParamsAreNotProvided() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly"};

        Map<String, String> results = paramResolver.resolve(params);

        assertThat(results).isEmpty();
    }
}
