package com.ef.parameter;

import org.junit.Test;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class ParameterFacadeTest {

    private ParameterFacade parameterFacade = new ParameterFacade();

    @Test
    public void shouldUseFacadeToParseParameters() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=100"};

        Map<String, String> results = parameterFacade.resolveParameters(params);

        assertThat(results).containsKeys("accesslog", "startDate", "duration", "threshold").containsValues("/path/to/file", "2017-01-01.13:00:00", "hourly", "100").size().isEqualTo(4);
    }

    @Test
    public void shouldUseFacadeToNotResolveAllParametersWhenMoreThanExpectedParamsAreProvided() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly", "--threshold=100", "--test=test"};

        Map<String, String> results = parameterFacade.resolveParameters(params);

        assertThat(results).isEmpty();
    }

    @Test
    public void shouldUseFacadeToNotResolveParametersWhenAllParamsAreNotProvided() {
        String[] params = new String[] {"--accesslog=/path/to/file", "--startDate=2017-01-01.13:00:00", "--duration=hourly"};

        Map<String, String> results = parameterFacade.resolveParameters(params);

        assertThat(results).isEmpty();
    }
}