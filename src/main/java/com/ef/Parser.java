package com.ef;

import com.ef.executor.Executor;
import com.ef.parameter.ParameterFacade;

import java.io.IOException;
import java.util.Map;

public class Parser {
    public static void main(String[] args) throws IOException {
        ParameterFacade parameterFacade = new ParameterFacade();
        Map<String, String> parameters = parameterFacade.resolveParameters(args);

        Executor executor = new Executor();
        executor.execute(parameters);
    }
}
