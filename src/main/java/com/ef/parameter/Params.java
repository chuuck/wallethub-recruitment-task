package com.ef.parameter;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
enum Params {
    ACCESS_LOG("a", "accesslog", true, "Path to the accesslog"),
    START_DATE("s", "startDate", true, "Start date for log research"),
    DURATION("d", "duration", true, "Duration of range in research"),
    THRESHOLD("t", "threshold", true, "Number of requests made by certain ip address");

    private String shortParamName;
    private String paramName;
    private boolean argProvided;
    private String description;
}
