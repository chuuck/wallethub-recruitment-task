package com.ef.parameter;

import com.google.common.collect.Maps;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

class ParamResolver {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParamResolver.class);

    public Map<String, String> resolve(String[] params) {
        Options options = prepareCommandLineOptions();

        CommandLine commandLine = parseParams(params, options);
        if (null == commandLine) {
            return Maps.newHashMap();
        }

        return this.prepareResult(commandLine.getOptions());
    }

    private CommandLine parseParams(String[] params, Options options) {
        DefaultParser defaultParser = new DefaultParser();
        CommandLine commandLine = null;

        try {
            commandLine = defaultParser.parse(options, params);
        } catch (ParseException e) {
            LOGGER.error(e.getMessage());
        }

        return commandLine;
    }

    private Options prepareCommandLineOptions() {
        Options options = new Options();

        for (Params param : Params.values()) {
            Option option = new Option(param.getShortParamName(), param.getParamName(), param.isArgProvided(), param.getDescription());
            if (!Params.ACCESS_LOG.getParamName().equalsIgnoreCase(param.getParamName())) {
                option.setRequired(true);
            }
            options.addOption(option);
        }

        return options;
    }

    private Map<String, String> prepareResult(Option[] options) {
        Map<String, String> result = new HashMap<>();
        for (Option option : options) {
            result.put(option.getLongOpt(), option.getValue());
        }

        return result;
    }
}
