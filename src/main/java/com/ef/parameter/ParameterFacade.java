package com.ef.parameter;

import java.util.Map;

public class ParameterFacade {

    private final ParamResolver paramResolver;

    public ParameterFacade() {
        this.paramResolver = new ParamResolver();
    }

    public Map<String, String> resolveParameters(String[] params) {
        return this.paramResolver.resolve(params);
    }
}
