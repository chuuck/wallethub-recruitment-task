package com.ef.repository;

import com.ef.ip.persistence.IPAddress;
import com.ef.log.persistence.Log;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.logging.Level;

public abstract class AbstractRepository {
    private static SessionFactory sessionFactory = null;

    protected static Session openSession() {
        disableLogging();
        if (null == sessionFactory) {
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Log.class).addAnnotatedClass(IPAddress.class).buildSessionFactory();
        }
        return sessionFactory.openSession();
    }

    protected void clear(Session session) {
        session.clear();
        session.close();
    }

    protected void removeAll(String tableName) {
        String stringQuery = "DELETE FROM " + tableName;
        Session session = openSession();
        session.getTransaction().begin();
        Query query = session.createQuery(stringQuery);
        query.executeUpdate();
        session.getTransaction().commit();
        clear(session);
    }

    private static void disableLogging() {
        //In real world this wouldn't be off but for command line app execution in this recruitment task it will be fine to have clear view (see also resources/logging.properties)
        java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.OFF);
    }
}
