package com.ef.executor;

import com.ef.ip.persistence.IPAddress;
import com.ef.log.persistence.Log;
import com.ef.log.persistence.LogQuery;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataProcessor extends AbstractProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataProcessor.class);

    public void process(Map<String, String> parameters) {
        if (parameters.containsKey("duration") && parameters.containsKey("threshold") && parameters.containsKey("startDate")) {
            String duration = parameters.get("duration");
            String paramDate = parameters.get("startDate").replaceFirst("\\.", " ");
            int threshold = Integer.valueOf(parameters.get("threshold"));

            List<Log> result = invokeQuery(duration, paramDate);
            analyzeBlockedIPs(threshold, result);
        }
    }

    private List<Log> invokeQuery(String duration, String paramDate) {
        LocalDateTime startDate = new LocalDateTime(Timestamp.valueOf(paramDate));
        LocalDateTime endDate = calculateEndDate(paramDate, duration);

        return findLogsUsingLogQuery(startDate, endDate);
    }

    private List<Log> findLogsUsingLogQuery(LocalDateTime startDate, LocalDateTime endDate) {
        LogQuery logQuery = new LogQuery(startDate, endDate);

        return this.logFacade.findLogsUsingLogQuery(logQuery);
    }

    private void analyzeBlockedIPs(int threshold, List<Log> result) {
        LOGGER.info("Blocked IPs: ");
        Map<String, List<IPAddress>> grouppedIPs = groupLogsByIPAddress(result);
        displayBlockedIPs(threshold, grouppedIPs);
        LOGGER.info("Blocked IPs displayed.");
    }

    private Map<String, List<IPAddress>> groupLogsByIPAddress(List<Log> result) {
        return result
                .stream()
                .map(log -> log.getIpAddress())
                .collect(Collectors.groupingBy(ip -> ip.getIp()));
    }

    private void displayBlockedIPs(int threshold, Map<String, List<IPAddress>> grouppedIPs) {
        grouppedIPs
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().size() > threshold)
                .forEach(entry -> {
                    System.out.println(entry.getValue().get(0).getIp());
                    this.ipAddressFacade.makeIPAddressBlocked(entry.getValue().get(0));
                });
    }

    private LocalDateTime calculateEndDate(String parameter, String duration) {
        LocalDateTime endDate = new LocalDateTime(Timestamp.valueOf(parameter));
        if (Duration.HOURLY.name().equalsIgnoreCase(duration)) {
            endDate = endDate.plusHours(1);
        }
        else if (Duration.DAILY.name().equalsIgnoreCase(duration)) {
            endDate = endDate.plusDays(1);
        }

        return endDate;
    }

    private enum Duration {
        DAILY, HOURLY
    }
}
