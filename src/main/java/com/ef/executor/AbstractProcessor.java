package com.ef.executor;

import com.ef.ip.IPAddressFacade;
import com.ef.log.LogFacade;

import java.io.IOException;
import java.util.Map;

abstract class AbstractProcessor {

    protected final LogFacade logFacade;
    protected final IPAddressFacade ipAddressFacade;

    public AbstractProcessor() {
        this.logFacade = new LogFacade();
        this.ipAddressFacade = new IPAddressFacade();
    }

    abstract void process(Map<String, String> parameters) throws IOException;
}
