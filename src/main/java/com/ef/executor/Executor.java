package com.ef.executor;

import com.ef.ip.IPAddressFacade;
import com.ef.ip.persistence.IPAddress;
import com.ef.log.LogFacade;
import com.ef.log.persistence.Log;
import com.ef.log.persistence.LogQuery;
import com.google.common.base.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Executor {

    private static final Logger LOGGER = LoggerFactory.getLogger(Executor.class);
    private final LogProcessor logProcessor;
    private final DataProcessor dataProcessor;

    public Executor() {
        this.logProcessor = new LogProcessor();
        this.dataProcessor = new DataProcessor();
    }

    public void execute(Map<String, String> parameters) throws IOException {
        this.logProcessor.process(parameters);
        this.dataProcessor.process(parameters);
        System.exit(0);
    }
}
