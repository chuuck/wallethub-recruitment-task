package com.ef.executor;

import com.ef.ip.persistence.IPAddress;
import com.ef.log.persistence.Log;
import com.google.common.base.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Map;

class LogProcessor extends AbstractProcessor {

    private static final Logger LOGGER = LoggerFactory.getLogger(LogProcessor.class);

    public void process(Map<String, String> parameters) throws IOException {
        if (parameters.containsKey("accesslog") && StringUtils.isNotEmpty(parameters.get("accesslog"))) {
            LOGGER.info("Loading and persisting logs...");
            String path = parameters.get("accesslog");
            LineIterator lineIterator = FileUtils.lineIterator(new File(path), Charsets.UTF_8.toString());
            while (lineIterator.hasNext()) {
                String rawLog = lineIterator.nextLine();
                Log log = this.logFacade.mapToLog(rawLog);
                persistLog(log);
            }
        }
    }

    private void persistLog(Log log) {
        IPAddress ipAddress = log.getIpAddress();

        IPAddress existingIpAddress = this.ipAddressFacade.findByIPAddress(ipAddress.getIp());
        if (null == existingIpAddress) {
            ipAddress.addLog(log);
            this.ipAddressFacade.insertIPAddress(ipAddress);

            log.setIpAddress(ipAddress);
        } else {
            log.setIpAddress(existingIpAddress);
        }

        this.logFacade.insertLog(log);
    }
}
