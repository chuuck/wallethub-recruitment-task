package com.ef.log.persistence;

import com.ef.ip.persistence.IPAddress;
import com.google.common.base.Objects;
import lombok.*;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

@Entity(name = "log")
@Getter
@Setter
@NoArgsConstructor
@ToString(exclude = "ipAddress")
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime date;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn
    private IPAddress ipAddress;

    private String request;

    private String status;

    private String userAgent;
}
