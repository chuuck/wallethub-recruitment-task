package com.ef.log.persistence;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.joda.time.LocalDateTime;

@AllArgsConstructor
@Getter
public class LogQuery {

    private LocalDateTime startDate;
    private LocalDateTime endDate;

}
