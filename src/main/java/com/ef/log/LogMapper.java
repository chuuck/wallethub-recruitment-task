package com.ef.log;

import com.ef.ip.persistence.IPAddress;
import com.ef.log.dto.LogEntry;
import com.ef.log.persistence.Log;
import com.google.common.collect.Sets;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Arrays;
import java.util.Map;

class LogMapper {
    public LogEntry mapToLogEntry(Map<String, String> dataMap) {
        return new LogEntry(
                this.mapLocalDateTime(dataMap.get(LogChunks.DATE.toString())),
                dataMap.get(LogChunks.IP_ADDRESS.toString()),
                dataMap.get(LogChunks.REQUEST.toString()),
                dataMap.get(LogChunks.STATUS.toString()),
                dataMap.get(LogChunks.USER_AGENT.toString())
        );
    }

    public Log mapToLog(LogEntry mappedLog) {
        Log log = new Log();
        log.setIpAddress(new IPAddress(mappedLog.getIpAddress(), false, ""));
        log.setDate(mappedLog.getDate());
        log.setStatus(mappedLog.getStatus());
        log.setRequest(mappedLog.getRequest());
        log.setUserAgent(mappedLog.getUserAgent());

        return log;
    }

    private LocalDateTime mapLocalDateTime(String localDateTime) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
        return dateTimeFormatter.parseLocalDateTime(localDateTime);
    }
}
