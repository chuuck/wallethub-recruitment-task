package com.ef.log;

import org.apache.commons.lang.StringUtils;

import java.util.Map;
import java.util.stream.Collectors;

class LogValidator {
    public boolean validate(Map<String, String> dataMap) {
        Map<String, String> dataToValidate = dataMap.entrySet().stream().filter(entry -> StringUtils.isNotEmpty(LogChunks.valueOf(entry.getKey()).getRegexPattern())).collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue()));

        for (Map.Entry entry : dataToValidate.entrySet()) {
            String regexPattern = LogChunks.valueOf((String) entry.getKey()).getRegexPattern();
            String value = (String) entry.getValue();

            if (!value.matches(regexPattern)) {
                return false;
            }
        }

        return true;
    }
}
