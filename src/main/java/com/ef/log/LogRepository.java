package com.ef.log;

import com.ef.log.persistence.Log;
import com.ef.repository.AbstractRepository;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;
import org.joda.time.LocalDateTime;

import javax.persistence.criteria.*;
import java.util.List;

class LogRepository extends AbstractRepository {

    private static final String TABLE_NAME = "log";

    public void insertLog(Log log) {
        Session session = openSession();
        session.getTransaction().begin();
        session.persist(log);
        session.getTransaction().commit();
        clear(session);
    }

    public Log findLogById(Long id) {
        Session session = openSession();
        Log log = session.find(Log.class, id);

        clear(session);
        return log;
    }

    public List<Log> findLogsBetweenDates(LocalDateTime startDate, LocalDateTime endDate) {
        Session session = openSession();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Log> query = criteriaBuilder.createQuery(Log.class);
        Root<Log> from = query.from(Log.class);
        Predicate condition = criteriaBuilder.between(from.get("date"), startDate, endDate);
        query.where(condition);

        List<Log> resultList = session.createQuery(query).getResultList();
        clear(session);
        return resultList;
    }

    /**
     * !!! TESTED ONLY MANUALLY BECAUSE OF LEFT JOIN !!!
     * To obtain objectively and programatically IPAddress object based on Log object from our database
     * we have to use more java than sql to serve that, just simply pulling IPAddress object from Log object got from database.
     *
     * @param startDate
     * @param endDate
     * @param limit
     * @return
     *
     * query example
     * SELECT i.*, COUNT(l.ipAddress_id) as counted FROM log l LEFT JOIN ip_address i ON i.id=l.ipAddress_id WHERE date BETWEEN '2017-01-01 01:00:12.763' AND '2017-01-01 01:00:59.410' GROUP BY ipAddress_id HAVING count(ipAddress_id) > 2;
     */
    public List<Object> findIPAddressBetweenDatesWithLimit(LocalDateTime startDate, LocalDateTime endDate, int limit) {
        Session session = openSession();
        String sql = "SELECT i.*, COUNT(l.ipAddress_id) as counted FROM log l LEFT JOIN ip_address i ON i.id = l.ipAddress_id WHERE date BETWEEN :startDate AND :endDate GROUP BY ipAddress_id HAVING count(ipAddress_id) > :limit;";
        NativeQuery nativeQuery = session.createNativeQuery(sql);
        nativeQuery.setParameter("startDate", startDate.toString());
        nativeQuery.setParameter("endDate", toString());
        nativeQuery.setParameter("limit", limit);

        List resultList = nativeQuery.getResultList();
        clear(session);
        return resultList;
    }

    /**
     * Write MySQL query to find requests made by a given IP.
     * @param ipAddress
     * @return
     *
     * query example
     * SELECT request FROM log WHERE id = (SELECT id FROM ip_address WHERE ip = '192.168.0.1');
     */
    public List<String> findRequestMadeByIPAddress(String ipAddress) {
        Session session = openSession();
        String sql = "SELECT request FROM log WHERE id = (SELECT id FROM ip_address WHERE ip = :ip);";
        NativeQuery nativeQuery = session.createNativeQuery(sql);
        nativeQuery.setParameter("ip", ipAddress);

        List resultList = nativeQuery.getResultList();
        clear(session);
        return resultList;
    }

    public void removeAll() {
        removeAll(TABLE_NAME);
    }
}
