package com.ef.log;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
enum LogChunks {
    DATE("(\\d{4})-(\\d{2})-(\\d{2}) (\\d{2}):(\\d{2}):(\\d{2}).(\\d+)"),
    IP_ADDRESS("\\b(?:\\d{1,3}\\.){3}\\d{1,3}\\b"),
    REQUEST,
    STATUS("[0-9][0-9][0-9]"),
    USER_AGENT;

    private String regexPattern;
}
