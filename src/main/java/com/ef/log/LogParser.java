package com.ef.log;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.Map;

class LogParser {

    private final LogValidator logValidator;

    public LogParser() {
        this.logValidator = new LogValidator();
    }

    public Map<String, String> parse(String log) {
        String[] parts = log.split("\\|");

        Map<String, String> dataMap = this.constructDataMap(parts);

        boolean isDataMapValid = this.logValidator.validate(dataMap);
        if (isDataMapValid) {
            return dataMap;
        }

        return Maps.newHashMap();
    }

    private Map<String, String> constructDataMap(String[] parts) {
        HashMap<String, String> dataMap = Maps.newHashMap();
        if (parts.length == 5) {
            dataMap.put(LogChunks.DATE.toString(), parts[0]);
            dataMap.put(LogChunks.IP_ADDRESS.toString(), parts[1]);
            dataMap.put(LogChunks.REQUEST.toString(), StringUtils.substringBetween(parts[2], "\"", "\""));
            dataMap.put(LogChunks.STATUS.toString(), parts[3]);
            dataMap.put(LogChunks.USER_AGENT.toString(), StringUtils.substringBetween(parts[4], "\"", "\""));
        }
        return dataMap;
    }
}
