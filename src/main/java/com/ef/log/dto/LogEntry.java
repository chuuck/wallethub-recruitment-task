package com.ef.log.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.joda.time.LocalDateTime;


@AllArgsConstructor
@Getter
public class LogEntry {
    private LocalDateTime date;
    private String ipAddress;
    private String request;
    private String status;
    private String userAgent;
}
