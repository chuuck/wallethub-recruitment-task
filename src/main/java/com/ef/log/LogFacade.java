package com.ef.log;

import com.ef.log.dto.LogEntry;
import com.ef.log.persistence.Log;
import com.ef.log.persistence.LogQuery;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LogFacade {

    private final LogRepository logRepository;
    private final LogParser logParser;
    private final LogMapper logMapper;

    public LogFacade() {
        this.logRepository = new LogRepository();
        this.logParser = new LogParser();
        this.logMapper = new LogMapper();
    }

    public void insertLog(Log log) {
        this.logRepository.insertLog(log);
    }

    public List<Log> findLogsUsingLogQuery(LogQuery logQuery) {
        return this.logRepository.findLogsBetweenDates(logQuery.getStartDate(), logQuery.getEndDate());
    }

    public Log mapToLog(String log) {
        Map<String, String> dataMap = this.logParser.parse(log);
        LogEntry mappedLog = this.logMapper.mapToLogEntry(dataMap);

        return this.logMapper.mapToLog(mappedLog);
    }

}
