package com.ef.ip.persistence;

import com.ef.log.persistence.Log;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "ip_address")
@Data
@NoArgsConstructor
@ToString(exclude = "logs")
public class IPAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String ip;

    @Column(nullable = false, columnDefinition = "boolean default false")
    private Boolean blocked;

    private String comment;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "ipAddress", cascade = CascadeType.MERGE)
    private Set<Log> logs = new HashSet<>();

    public IPAddress(String ip, Boolean blocked, String comment) {
        this.ip = ip;
        this.blocked = blocked;
        this.comment = comment;
    }

    public void addLog(Log log) {
        this.logs.add(log);
    }
}
