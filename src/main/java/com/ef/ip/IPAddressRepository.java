package com.ef.ip;

import com.ef.ip.persistence.IPAddress;
import com.ef.repository.AbstractRepository;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

class IPAddressRepository extends AbstractRepository {

    private static final String TABLE_NAME = "ip_address";

    public void insertIPAddress(IPAddress ipAddress) {
        Session session = openSession();
        session.getTransaction().begin();
        session.persist(ipAddress);
        session.getTransaction().commit();
        clear(session);
    }

    public IPAddress findIPAddressById(Long id) {
        Session session = openSession();
        IPAddress ipAddress = session.find(IPAddress.class, id);

        clear(session);
        return ipAddress;
    }

    public IPAddress findByIPAddress(String ipAddress) {
        Session session = openSession();

        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<IPAddress> query = criteriaBuilder.createQuery(IPAddress.class);
        Root<IPAddress> from = query.from(IPAddress.class);
        Predicate condition = criteriaBuilder.equal(from.get("ip"), ipAddress);
        query.where(condition);

        List<IPAddress> resultList = session.createQuery(query).getResultList();
        clear(session);
        return !resultList.isEmpty() ? resultList.get(0) : null;
    }

    public void updateIPAddress(IPAddress ipAddress) {
        Session session = openSession();
        session.getTransaction().begin();
        session.saveOrUpdate(ipAddress);
        session.getTransaction().commit();
        clear(session);
    }

    public void removeAll() {
        removeAll(TABLE_NAME);
    }
}
