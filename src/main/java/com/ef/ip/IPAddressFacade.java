package com.ef.ip;

import com.ef.ip.persistence.IPAddress;

public class IPAddressFacade {

    private final IPAddressRepository ipAddressRepository;

    public IPAddressFacade() {
        this.ipAddressRepository = new IPAddressRepository();
    }

    public void insertIPAddress(IPAddress ipAddress) {
        this.ipAddressRepository.insertIPAddress(ipAddress);
    }

    public IPAddress findByIPAddress(String ipAddress) {
        return this.ipAddressRepository.findByIPAddress(ipAddress);
    }

    public void makeIPAddressBlocked(IPAddress ipAddress) {
        ipAddress.setBlocked(true);
        ipAddress.setComment("Blocked due to too much requests.");
        this.ipAddressRepository.updateIPAddress(ipAddress);
    }
}
